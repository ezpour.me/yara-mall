from rest_framework import permissions


# class IsOwnerOrReadOnly(permissions.BasePermission):
#     def has_permission(self, request, view):
#         pass
#
#     def has_object_permission(self, request, view, obj):
#         pass
#

class BaseSeller(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS or request.user.is_superuser:
            return True
        elif hasattr(request.user, 'seller'):
            return True
        else:
            return False


class StorePermission(BaseSeller):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            return obj.user == request.user or request.user.is_superuser


class ProductPermission(BaseSeller):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            return obj.store.user == request.user or request.user.is_superuser
