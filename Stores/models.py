from django.db import models
from django.utils.text import slugify

from Accounts.models import User


class Store(models.Model):
    user = models.ForeignKey(to=User, verbose_name='owner store', on_delete=models.CASCADE, related_name='stores')
    name = models.CharField(verbose_name='store name', max_length=150)
    slug = models.SlugField(verbose_name="Slug", max_length=200)
    activity_code = models.CharField(verbose_name='activity code', max_length=20)
    address = models.CharField(verbose_name='address', max_length=250)
    description = models.TextField(verbose_name='description', null=True, blank=True)
    is_active = models.BooleanField(verbose_name='available', default=True)
    created = models.DateTimeField(verbose_name='created', auto_now_add=True)

    class Meta:
        ordering = ['-created']
        verbose_name = 'store'
        verbose_name_plural = "stores"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.name, allow_unicode=True)
        super(Store, self).save(*args, **kwargs)


class Product(models.Model):
    store = models.ForeignKey(to=Store, verbose_name='store', on_delete=models.PROTECT, related_name='products')
    name = models.CharField(verbose_name='name', max_length=50)
    slug = models.SlugField(verbose_name="Slug", max_length=200)
    price = models.PositiveSmallIntegerField(verbose_name='price')
    count = models.PositiveSmallIntegerField(verbose_name='count')
    is_active = models.BooleanField(verbose_name='available', default=True)
    created = models.DateField(verbose_name='date create', auto_now_add=True)

    class Meta:
        ordering = ['-created']
        verbose_name = 'product'
        verbose_name_plural = "products"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.name, allow_unicode=True)
        super(Product, self).save(*args, **kwargs)


class File(models.Model):
    product = models.ForeignKey(to=Product, verbose_name='product', on_delete=models.CASCADE, related_name='files')
    name = models.CharField(verbose_name='name', max_length=120)
    file_address = models.FileField(verbose_name='file')
    created_time = models.DateTimeField(verbose_name="create Time", auto_now_add=True)
    update_time = models.DateTimeField(verbose_name="update Time", auto_now=True)

    def __str__(self):
        return self.name
