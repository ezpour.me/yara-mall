from django.contrib import admin

from Stores.models import Store, Product, File


@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
    pass


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass


@admin.register(File)
class FileAdmin(admin.ModelAdmin):
    pass
