from rest_framework import serializers

from Stores.models import Store, Product, File


class ProductListSerializer(serializers.HyperlinkedModelSerializer):
    store_name = serializers.StringRelatedField(source='store.name')

    class Meta:
        model = Product
        fields = ['name', 'count', 'price', 'store', 'store_name', 'url']
        read_only_fields = ['store_name']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'},
            'store': {'lookup_field': 'slug'}
        }


class ProductMainSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['name', 'count', 'price', 'store']


class ProductDetailSerializer(serializers.HyperlinkedModelSerializer):
    owner_store = serializers.SerializerMethodField()
    user_link = serializers.HyperlinkedRelatedField(view_name='user-detail', source='store.user', read_only=True)

    class Meta:
        model = Product
        fields = ['id', 'name', 'count', 'price', 'store', 'owner_store', 'user_link', 'is_active']
        read_only_fields = ['store', 'owner_store']
        extra_kwargs = {
            'store': {'lookup_field': 'slug'}
        }

    def get_owner_store(self, obj):
        return obj.store.user.username


class StoreListSerializer(serializers.ModelSerializer):
    username = serializers.StringRelatedField(source='user')
    user = serializers.HyperlinkedIdentityField(view_name='user-detail')

    class Meta:
        model = Store
        fields = ['id', 'name', 'url', 'user', 'username']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class StoreMainSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = ['id', 'name', 'activity_code', 'address', 'description']

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return super(StoreMainSerializer, self).create(validated_data)


class StoreDetailsSerializer(serializers.ModelSerializer):
    username = serializers.StringRelatedField(source='user')
    user = serializers.HyperlinkedIdentityField(view_name='user-detail')

    class Meta:
        lookup_field = 'slug'
        model = Store
        fields = ['id', 'name', 'user', 'username', 'activity_code',
                  'address', 'description', 'is_active', 'created']
        read_only_fields = ['user', 'username', 'created']


class FileMainSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = '__all__'


