from django.db.models import ProtectedError
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from YaraMall.throttles import CreateStoreRateThrottle, CreateProductRateThrottle
from .custom_search import StoreFilter, ProductFilter
from .permissions import StorePermission, ProductPermission

from Stores.models import Store, Product, File
from Stores.serializers import StoreMainSerializer, StoreDetailsSerializer, FileMainSerializer, \
    StoreListSerializer, ProductDetailSerializer, ProductListSerializer, ProductMainSerializer


class StoreViewSet(viewsets.ModelViewSet):
    serializer_class = StoreMainSerializer
    queryset = Store.objects.filter(is_active=True)
    permission_classes = [StorePermission, ]
    lookup_field = 'slug'
    filterset_class = StoreFilter

    def get_throttles(self):
        if self.action == 'create':
            self.throttle_classes = [CreateStoreRateThrottle]
        return super(StoreViewSet, self).get_throttles()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            self.serializer_class = StoreDetailsSerializer
        if self.action == 'list':
            self.serializer_class = StoreListSerializer
        return super(StoreViewSet, self).get_serializer_class()
    
    @method_decorator(cache_page(60))
    def list(self, request, *args, **kwargs):
        return super(StoreViewSet, self).list(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        try:
            super(StoreViewSet, self).destroy(request, *args, **kwargs)
            return Response({'message': 'ok'})
        except ProtectedError:
            store = self.get_object()
            store.is_active = False
            store.save()
            return Response({'message': 'this store is under protect but this product now id unaccessAble'})

    @action(detail=False)
    def get_my_store(self, request):
        store_list = self.request.user.stores.all()
        return Response(StoreMainSerializer(store_list, many=True).data)

    @action(detail=True)
    def products(self, request, slug):
        a = Product.objects.filter(store__slug=slug)
        page = self.paginate_queryset(a)
        if page is not None:
            return self.get_paginated_response(
                ProductMainSerializer(instance=page, many=True).data
            )

        data = ProductMainSerializer(instance=a, many=True).data
        return Response(data)


class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductMainSerializer
    queryset = Product.objects.all()
    permission_classes = [ProductPermission, ]
    lookup_field = 'slug'
    filterset_class = ProductFilter

    def get_throttles(self):
        if self.action == 'update':
            self.throttle_classes = [CreateProductRateThrottle]
        return super(ProductViewSet, self).get_throttles()
    
    def get_serializer_class(self):
        if self.action == 'list':
            self.serializer_class = ProductListSerializer
        elif self.action == 'retrieve':
            self.serializer_class = ProductDetailSerializer

        return super(ProductViewSet, self).get_serializer_class()

    # @method_decorator(cache_page(60))
    def list(self, request, *args, **kwargs):
        return super(ProductViewSet, self).list(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        try:
            super(ProductViewSet, self).destroy(request, *args, **kwargs)
            return Response({'message': 'ok'})
        except ProtectedError:
            product = self.get_object()
            product.is_active = False
            product.save()
            return Response({'message': 'this product is under protect but this product now id unaccessAble'})


class FileViewSet(viewsets.ModelViewSet):
    serializer_class = FileMainSerializer
    queryset = File.objects.all()