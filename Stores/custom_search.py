from django_filters import rest_framework as filters

from Stores.models import Store, Product


class StoreFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_expr='contains')
    user = filters.CharFilter(field_name='user__username', lookup_expr='contains')

    class Meta:
        model = Store
        fields = ['name', 'user', 'is_active']


class ProductFilter(filters.FilterSet):
    min_price = filters.NumberFilter(field_name="price", lookup_expr='gte')
    max_price = filters.NumberFilter(field_name="price", lookup_expr='lte')
    name = filters.CharFilter(lookup_expr='contains')
    user = filters.CharFilter(field_name='store__user__username', lookup_expr='contains')
    store = filters.CharFilter(field_name='store__name', lookup_expr='contains')

    class Meta:
        model = Product
        fields = ['name', 'store', 'user', 'is_active', 'min_price', 'max_price']
