from rest_framework import serializers

from Accounts.models import User


class AccountMainSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
