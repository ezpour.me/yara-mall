from rest_framework import viewsets

from Accounts.models import User
from Accounts.serializers import AccountMainSerializer


class AccountViewSet(viewsets.ModelViewSet):
    serializer_class = AccountMainSerializer
    queryset = User.objects.all()
