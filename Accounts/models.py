from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    pass


class Seller(models.Model):
    user = models.OneToOneField(to=User, on_delete=models.CharField, related_name='seller')
    is_active = models.BooleanField(default=False)
