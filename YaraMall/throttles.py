from django.core.cache import cache
from rest_framework import throttling


class CreateRateThrottle(throttling.BaseThrottle):
    name = 'test'

    def allow_request(self, request, view):
        a = cache.get_or_set(request.user.username+'-'+self.name, 10, 100)
        if a == 0:
            return False
        cache.decr(request.user.username+'-'+self.name)
        return True


class CreateStoreRateThrottle(CreateRateThrottle):
    name = 'CreateStore'


class CreateProductRateThrottle(CreateRateThrottle):
    name = 'CreateProduct'
