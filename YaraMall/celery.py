from __future__ import absolute_import, unicode_literals
import os

from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'YaraMall.settings')

# other shared tasks
app = Celery('YaraMall')

app.config_from_object('django.conf:settings', namespace='CELERY')

# Prevent from converting task times into UTC
app.conf.enable_utc = False
app.conf.timezone = 'Asia/Tehran'
# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
