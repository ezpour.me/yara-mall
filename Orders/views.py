from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from django.utils import timezone
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from Orders.models import Basket, BasketItem
from Orders.custom_search import BasketFilter
from Orders.serializers import BasketItemMainSerializer, BasketDetailsSerializer, BasketListSerializer, \
    BasketItemListSerializer
from Orders.tasks import send_email_after_payment
from Stores.models import Product


class BasketViewSet(viewsets.ModelViewSet):
    queryset = Basket.objects.all()
    http_method_names = ['get', 'patch']
    serializer_class = BasketListSerializer
    # search_fields = ['factor_code']
    filterset_class = BasketFilter

    # search_fields = ['factor_code',]

    def get_queryset(self):
        if self.action == 'list':
            return self.request.user.basket.filter(status=True)
        return self.request.user.basket.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            self.serializer_class = BasketDetailsSerializer
        return super(BasketViewSet, self).get_serializer_class()

    @action(detail=False)
    def get_active_basket(self, request):
        basket = self.request.user.basket.filter(status=False).first()
        if not basket:
            return Response({'message': 'you dont have basket.'})
        return Response(BasketDetailsSerializer(basket, context={'request': self.request}).data)

    @action(detail=False)
    def payment(self, request):
        mybasket = Basket.objects.get(user=request.user, status=False)
        products = Product.objects.filter(basket_item__basket=mybasket)
        new_price = list(products.values_list('price', flat=True))
        items = mybasket.basket_item.all()

        # update price product in basket item
        stack_query = []
        new_total_price = 0
        for i in range(items.count()):
            j = items[i]
            if items[i].price != new_price[i]:
                j.price = new_price[i]
                stack_query.append(j)
            new_total_price += (j.count * j.price)
        if stack_query:
            BasketItem.objects.bulk_update(stack_query, ['price'])

        # update total price in basket
        if mybasket.total_price != new_total_price:
            mybasket.total_price = new_total_price
            mybasket.save()
            return Response({'message': 'your basket updated please try again.'})
        mybasket.status = True
        mybasket.created = timezone.datetime.now()
        mybasket.save()
        send_email_after_payment.delay(mybasket.id)
        return Response({'message': 'success payment'})

    @action(detail=False)
    def delete_basket(self, request):
        item = self.request.user.basket.filter(status=False).first()
        if not item:
            return Response({'message': 'you dont have any basket to delete'})

        item.delete()
        return Response({'message': 'basket deleted'})


class BasketItemViewSet(viewsets.ModelViewSet):
    serializer_class = BasketItemMainSerializer
    queryset = BasketItem.objects.all()
    http_method_names = ['delete', 'post', 'patch']
    
    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve':
            self.serializer_class = BasketItemListSerializer
        return super(BasketItemViewSet, self).get_serializer_class()
    
    def get_queryset(self):
        basket = self.request.user.basket.filter(status=False).first()
        if not basket:
            basket = Basket(user=self.request.user)
            basket.save()
        return basket.basket_item.all()
