from rest_framework import serializers
from rest_framework.generics import get_object_or_404

from Orders.models import Basket, BasketItem


class BasketItemMainSerializer(serializers.ModelSerializer):
    class Meta:
        model = BasketItem
        fields = ['count', 'product']

    def get_basket(self):
        # get basket
        list_item = Basket.objects.filter(status=False, user=self.context.get("request").user).first()
        if list_item:
            basket = list_item
        else:
            basket = Basket(user=self.context.get("request").user)
            basket.save()
        return basket

    def create(self, validated_data):

        # check available product in store
        product = validated_data['product']
        if validated_data['count'] > product.count or not (product.store.is_active and product.is_active):
            raise Exception('not count available in store ')

        basket = self.get_basket()

        # if product exist in basket increase count
        item = basket.basket_item.filter(product=product.id)

        if item:
            item = item[0]
            item.count += validated_data['count']
            if item.count > product.count:
                raise Exception('not count available in store ')
            item.save()
        else:
            validated_data['basket'] = basket
            item = super(BasketItemMainSerializer, self).create(validated_data)
        basket.total_price += (validated_data['count'] * product.price)
        basket.save()
        return item

    def update(self, instance, validated_data):
        basket = self.get_basket()
        item = get_object_or_404(basket.basket_item, product=instance.product)
        if item.count < validated_data['count']:
            item.count = 0
        else:
            item.count -= validated_data['count']
        item.save()
        return item


class BasketItemListSerializer(serializers.HyperlinkedModelSerializer):
    product_name = serializers.StringRelatedField(source='product.name')

    class Meta:
        model = BasketItem
        fields = ['id', 'product_name', 'count', 'price', 'product']
        read_only_fields = ['price', 'id', 'product_name']
        extra_kwargs = {
            'product': {'lookup_field': 'slug'}
        }


class BasketListSerializer(serializers.ModelSerializer):

    class Meta:

        model = Basket
        fields = ['id', 'user', 'factor_code', 'tracking_code',
                  'total_price', 'status', 'created', 'url']


class BasketDetailsSerializer(serializers.ModelSerializer):
    basket_item = serializers.ListSerializer(child=BasketItemListSerializer())

    class Meta:
        model = Basket
        fields = ['id', 'user', 'factor_code', 'tracking_code',
                  'total_price', 'status', 'created', 'basket_item']
