from django_filters import rest_framework as filters
from Orders.models import Basket


class BasketFilter(filters.FilterSet):
    min_price = filters.NumberFilter(field_name="total_price", lookup_expr='gte')
    max_price = filters.NumberFilter(field_name="total_price", lookup_expr='lte')
    factor_code = filters.CharFilter(lookup_expr='contains')

    class Meta:
        model = Basket
        fields = ['factor_code', 'min_price', 'max_price']
