from time import sleep

from celery import shared_task
from celery.schedules import crontab
from celery.task import periodic_task
# from celery.utils.time import timezone
from django.core.mail import send_mail
from django.utils import timezone

from Accounts.models import User
from Orders.models import Basket
from Orders.serializers import BasketListSerializer
from Stores.models import Product


@shared_task
def send_email_after_payment(data):
    basket = Basket.objects.get(pk=data)

    message = f'hi\n' \
              f'your factor is {basket.factor_code}, total price = {basket.total_price}'
    sleep(10)
    send_mail(
        'Payment',
        message,
        'from@example.com',
        [basket.user.email, basket.user.email],
    )


@shared_task
def send_email_after_delete_payment(data):
    user = User.objects.get(pk=data)

    message = f'sorry\n' \
              f'your factor deleted.'
    sleep(10)
    send_mail(
        'Delete Basket',
        message,
        'from@example.com',
        [user.email, user.email],
    )


@shared_task
def delete_basket_after_min(minutes):
    time_test = timezone.datetime.now() - timezone.timedelta(minutes=minutes)
    a = Basket.objects.filter(modify__lte=time_test, status=False)
    if a:
        b = list(a.values_list('user_id', flat=True))
        a.delete()

        for i in b:
            send_email_after_delete_payment.delay(i)
