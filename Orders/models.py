import uuid

from django.db import models

from Accounts.models import User
from Stores.models import Product


def generate_unique_code():
    return uuid.uuid4().hex[:6].upper()


class Basket(models.Model):
    user = models.ForeignKey(to=User, verbose_name='buyer user', on_delete=models.CASCADE, related_name='basket')
    basket_items = models.ManyToManyField(to=Product, through='BasketItem', verbose_name='basketItems')
    total_price = models.PositiveSmallIntegerField(verbose_name='total price', default=0)
    factor_code = models.CharField(verbose_name='factor code', max_length=6, default=generate_unique_code, unique=True)
    tracking_code = models.CharField(verbose_name='tracking code', max_length=25, null=True, blank=True)
    status = models.BooleanField(verbose_name='status payment', default=False)
    created = models.DateTimeField(verbose_name='register order', null=True)
    modify = models.DateTimeField(verbose_name='update', auto_now=True)

    class Meta:
        ordering = ['-created']
        verbose_name = 'basket'
        verbose_name_plural = "baskets"

    def __str__(self):
        return self.factor_code


class BasketItem(models.Model):
    basket = models.ForeignKey(to=Basket, verbose_name='basket', on_delete=models.CASCADE, related_name='basket_item')
    product = models.ForeignKey(to=Product, verbose_name='product', on_delete=models.PROTECT,
                                related_name='basket_item')
    price = models.PositiveSmallIntegerField(verbose_name='price')
    count = models.PositiveSmallIntegerField(verbose_name='count of product')
    created = models.DateTimeField(verbose_name='register in basket', auto_now_add=True)

    class Meta:
        ordering = ['-created']
        verbose_name = 'basket item'
        verbose_name_plural = "basket items"

    def delete(self, using=None, keep_parents=False):
        self.product.count += self.count
        self.product.save()
        return super(BasketItem, self).delete(using=None, keep_parents=False)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.price = self.product.price
            self.product.count -= self.count
            self.product.save()
        super(BasketItem, self).save(*args, **kwargs)
